document.getElementById('monedaO').onchange = function(){cambiar()};
document.getElementById('calcular').onclick = function(){calcular()};
document.getElementById('registro').onclick = function(){registrar()};
document.getElementById('borrar').onclick = function(){borrar()};

let cantidad = document.getElementById('cantidad');
let monedaOriginal = document.getElementById('monedaO');
let monedaDestino = document.getElementById('monedaD');

let totalC=0;
let subtotal=0;
let totalP=0;

let subtotalCampo = document.getElementById('subtotal');
let comision = document.getElementById('totalC');
let totalPagar = document.getElementById('totalP');

let registrarMoneda1 = '';
let registrarMoneda2 = '';
let registro = '';
let totalGeneral = 0;


function cambiar(){
    let original = document.getElementById('monedaO');
    let destino = document.getElementById('monedaD');
    let value = "";

    let opcionesO = "cadena";
    let opcionesD = "cadena";


    if(original.value == ''){
        opcionesO = '<option value=""></option><option id="1d" value="PesoMexicano">Peso Mexicano</option><option id="2d" value="DolarEstadounidense">Dolar Estadounidense</option><option id="3d" value="DolarCanadiense">Dolar Canadiense</option><option id="4d" value="Euro">Euro</option>';
        opcionesD = '<option value=""></option><option id="1d" value="PesoMexicano">Peso Mexicano</option><option id="2d" value="DolarEstadounidense">Dolar Estadounidense</option><option id="3d" value="DolarCanadiense">Dolar Canadiense</option><option id="4d" value="Euro">Euro</option>';
        value = "";
    }

    if(original.value == 'PesoMexicano'){
        opcionesO = '<option value=""></option><option id="1d" value="PesoMexicano">Peso Mexicano</option><option id="2d" value="DolarEstadounidense">Dolar Estadounidense</option><option id="3d" value="DolarCanadiense">Dolar Canadiense</option><option id="4d" value="Euro">Euro</option>';
        opcionesD = '<option value=""></option><option id="2d" value="DolarEstadounidense">Dolar Estadounidense</option><option id="3d" value="DolarCanadiense">Dolar Canadiense</option><option id="4d" value="Euro">Euro</option>';
        value = "PesoMexicano";
    }

    if(original.value == 'DolarEstadounidense'){
        opcionesO = '<option value=""></option><option id="1d" value="PesoMexicano">Peso Mexicano</option><option id="2d" value="DolarEstadounidense">Dolar Estadounidense</option><option id="3d" value="DolarCanadiense">Dolar Canadiense</option><option id="4d" value="Euro">Euro</option>';
        opcionesD = '<option value=""></option><option id="1d" value="PesoMexicano">Peso Mexicano</option><option id="3d" value="DolarCanadiense">Dolar Canadiense</option><option id="4d" value="Euro">Euro</option>';
        value = "DolarEstadounidense";
    }

    if(original.value == 'DolarCanadiense'){
        opcionesO = '<option value=""></option><option id="1d" value="PesoMexicano">Peso Mexicano</option><option id="2d" value="DolarEstadounidense">Dolar Estadounidense</option><option id="3d" value="DolarCanadiense">Dolar Canadiense</option><option id="4d" value="Euro">Euro</option>';
        opcionesD = '<option value=""></option><option id="1d" value="PesoMexicano">Peso Mexicano</option><option id="2d" value="DolarEstadounidense">Dolar Estadounidense</option><option id="4d" value="Euro">Euro</option>';
        value = "DolarCanadiense";
    }

    if(original.value == 'Euro'){
        opcionesO = '<option value=""></option><option id="1d" value="PesoMexicano">Peso Mexicano</option><option id="2d" value="DolarEstadounidense">Dolar Estadounidense</option><option id="3d" value="DolarCanadiense">Dolar Canadiense</option><option id="4d" value="Euro">Euro</option>';
        opcionesD = '<option value=""></option><option id="1d" value="PesoMexicano">Peso Mexicano</option><option id="2d" value="DolarEstadounidense">Dolar Estadounidense</option><option id="3d" value="DolarCanadiense">Dolar Canadiense</option>';
        value = "Euro";
    }

    

    original.innerHTML = opcionesO;
    original.value = value;
    destino.innerHTML = opcionesD;
};


function calcular(){
    
    /*Conviertiendo pesos mexicanos*/
    if(monedaOriginal.value == 'PesoMexicano'){
        if(monedaDestino.value == 'DolarEstadounidense'){
            subtotal = cantidad.value/19.85;
            registrarMoneda2 = "Dolar Estadounidense";
        }
        if(monedaDestino.value == 'DolarCanadiense'){
            subtotal = (cantidad.value/19.85)*1.35;
            registrarMoneda2 = "Dolar Canadiense";
        }
        if(monedaDestino.value == 'Euro'){
            subtotal = (cantidad.value/19.85)*.99;
            registrarMoneda2 = "Euro";
        }
        registrarMoneda1 = "Peso Mexicano";
    }

    /*Conviertiendo dolares estadounidenses*/
    if(monedaOriginal.value == 'DolarEstadounidense'){
        if(monedaDestino.value == 'PesoMexicano'){
            subtotal = cantidad.value*19.85;
            registrarMoneda2 = "Peso Mexicano";
        }
        if(monedaDestino.value == 'DolarCanadiense'){
            subtotal = cantidad.value*1.35;
            registrarMoneda2 = "Dolar Canadiense";
        }
        if(monedaDestino.value == 'Euro'){
            subtotal = cantidad.value*.99;
            registrarMoneda2 = "Euro";
        }
        registrarMoneda1 = "Dolar Estadounidense";
    }

    /*Conviertiendo dolares canadienses*/
    if(monedaOriginal.value == 'DolarCanadienses'){
        if(monedaDestino.value == 'PesoMexicano'){
            subtotal = (cantidad.value/1.35)*19.85;
            registrarMoneda2 = "Peso Mexicano";
        }
        if(monedaDestino.value == 'DolarEstadounidense'){
            subtotal = cantidad.value/1.35;
            registrarMoneda2 = "Dolar Estadounidense";
        }
        if(monedaDestino.value == 'Euro'){
            subtotal = (cantidad.value/1.35)*99;
            registrarMoneda2 = "Euro";
        }
        registrarMoneda1 = "Dolar Canadiense";
    }

    /*Conviertiendo euros*/
    if(monedaOriginal.value == 'Euro'){
        if(monedaDestino.value == 'PesoMexicano'){
            subtotal = (cantidad.value/.99)*19.85;
            registrarMoneda2 = "Peso Mexicano";
        }
        if(monedaDestino.value == 'DolarEstadounidense'){
            subtotal = (cantidad.value/.99);
            registrarMoneda2 = "Dolar Estadounidense";
        }
        if(monedaDestino.value == 'DolarCanadiense'){
            subtotal = subtotal = (cantidad.value/.99)*1.35;
            registrarMoneda2 = "Dolar Canadiense";
        }
        registrarMoneda1 = "Euro";
    }

    totalC = (subtotal*.3);
    totalP = subtotal + totalC;

    subtotalCampo.value = subtotal;
    comision.value = totalC;
    totalPagar.value = totalP;

}

function registrar(){
    registro = registro+"\n"+"///// Moneda original: "+registrarMoneda1+"   ////// Moneda de destino: "+registrarMoneda2+"      ///// Subtotal: "+subtotal+"      Comision: "+totalC+"    Total a pagar: "+totalP;

    document.getElementById('resultadoT').innerText = registro;

    totalGeneral = totalGeneral + totalP;

    document.getElementById('totalG').innerText = totalGeneral;
}

function borrar(){

    cantidad.value = 0;
    monedaOriginal.value = 0;
    monedaDestino.value = 0;

    subtotalCampo.value = 0;
    comision.value = 0;
    totalPagar.value = 0;

    registro = '';
    registrarMoneda1 = '';
    registrarMoneda2 = '';

    document.getElementById('resultadoT').innerText = '';
    document.getElementById('totalG').innerText = '';
}

